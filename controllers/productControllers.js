const Product = require('../models/Product');
const User = require('../models/User');


// Create a product
module.exports.createProductController = (req,res) => {

	console.log(req.body)

		Product.findOne({name: req.body.name},(err,result)=>{

			if(result !== null && result.name === req.body.name){

				return res.send({message:"Product is already registered"})
			} else {
				let newProduct = new Product ({

					name: req.body.name,
					description: req.body.description,
					price: req.body.price

				})

				newProduct.save((saveErr,savedProduct)=>{

					console.log(savedProduct);

					if(saveErr){

						return console.error(saveErr)
					} else {
						return res.send({message:`${savedProduct.name} has been added`,savedProduct})
					}
				})
			}
		})

}

// Retrieve all  Active Products
module.exports.retrieveActiveProduct = (req,res) => {

Product.find({isActive: true}).then(result =>  res.send(result)).catch(err => res.send(err))
	}

// Retrieve a single product
module.exports.retrieveSingleProduct = (req,res) => {

	console.log(req.params.id)
	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.updateProductController = (req,res) => {

	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err))
}

module.exports.archiveProductController = (req,res) => {

	
		let updates = {isActive: false}
	

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedProduct => res.send({message: 'Product has been archived',updatedProduct}))
	.catch(err => res.send(err))
}

module.exports.activateProductController = (req,res) => {

	
		let updates = {isActive: true}
	

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedProduct => res.send({message: 'Product has been active',updatedProduct}))
	.catch(err => res.send(err))
}

module.exports.displayProductPerOrder = (req,res) => {
if(req.user.isAdmin === true) return res.send({

	auth:"Failed",
	message: "Action Forbidden"

	});
Product.find({}).then(result =>  res.send(result)).catch(err => res.send(err))
	}
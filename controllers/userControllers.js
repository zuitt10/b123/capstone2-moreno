const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require("bcrypt");
const auth = require('../Authentication/auth');
const {createAccessToken} = auth;



module.exports.getSingleUserController = (req,res) =>{
	// login user's details after decoding with  module's verify()
	console.log(req.user);

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
}


// Register a User
module.exports.registerUserController = (req,res) => {
	// check if the user registered email is already registered
	User.findOne({email: req.body.email},(err,result)=>{


			if(result !== null && result.email === req.body.email){

				return res.send({message:"Email is already registered"});

			}

	// check if the registered password is less than 8. if password is less than 8 return an error message ("password is too short")
	else if(req.body.password.length < 8) return res.send({message: "Password is too short"})

	// set the bcrypy method in a variable hashedPW
	const hashedPW = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPW);
	// objects to be input in clients side
	let newUser = new User ({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo,
		email: req.body.email,
		password: hashedPW


	})
	//mongoose method - updates a document or insert a new document
	newUser.save()
	// for successful registration
	.then(user => res.send({message:`Thank you for registering ${req.body.firstName}`}))
	// catch the error
	.catch(err => res.send(err))
})
}
// Login
module.exports.loginUserController = (req,res) => {
	// mongoose method findOne, find the email and if the email is not found message the user "User not found"
	User.findOne({email: req.body.email})
	.then(result => {
		if(result === null){
			return res.send({message: "User not found."})
		// check if the password has bcrypt then add an access token. 
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,result.password);

			console.log(isPasswordCorrect)

			if(isPasswordCorrect){
				return  res.send({accessToken: createAccessToken(result)})
			} else{
				return res.send({message: "Password is incorrect"})
			}
		}
	})
	.catch(err => res.send(err))

}


module.exports.setUserAdminController = (req,res) => {

	let updates = {isAdmin: true}

	User.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedUser => res.send({message:"User is updated as an Admin"}))
	.catch(err => res.send(err))

}


module.exports.order = async (req,res) => {

	if(req.user.isAdmin === true) return res.send({

	auth:"Failed",
	message: "Action Forbidden"

	});

	let isUserOrder = await User.findById(req.user.id).then(user =>{
		// console.log(user)

		 user.order.push({	totalAmount: req.body.totalAmount,
							products: req.body.products
						 })
	

		return user.save()
		.then(user => {

			return user.order[user.order.length-1].id

		})
		
		.catch(err => false)



	
})

	console.log(isUserOrder)

	if(isUserOrder === false) return res.send(isUserOrder);

	// console.log(req.body.products[0].quantity);

	req.body.products.forEach( productDetail =>{
		// console.log(productDetail)

		Product.findById(productDetail.productId).then(product =>{

			// console.log(product)
			product.orders.push({orderId: isUserOrder,
								quantityOrder: productDetail.quantity
								})
			// console.log(product.orders)
			return product.save()

		
		})
	})

                                                    
	// console.log(isProductUpdated)
	// if(isProductUpdated !== true) return res.send(isProductUpdated);

	if(isUserOrder !== false) return res.send("order successfully")

} //end of setUserAdminCOntroll function


module.exports.viewOrderController = (req,res) => {
if(req.user.isAdmin === true) return res.send({

	auth:"Failed",
	message: "Action Forbidden"

	});

	User.findById(req.user.id)
	.then(result => res.send(result.order))
	.catch(err => res.send(err))

}

module.exports.viewAllOrderController = (req,res) => {
	User.find({},{_id:0,email:1,order:1})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}
const express = require('express');
const mongoose = require ('mongoose');
const cors = require('cors')
const app = express();
const port = process.env.PORT || 4000;

mongoose.connect("mongodb+srv://jericb123:Ravenger1@cluster0.9tqks.mongodb.net/BookingCapstone?retryWrites=true&w=majority",
{
	useNewUrlParser:true,
	useUnifiedTopology: true
}
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () =>console.log("Connected to MongoDb"));

app.use(cors());
app.use(express.json());

// http://localhost:4000/users
const userRoutes = require('./routes/userRoutes');
console.log(userRoutes);
app.use('/users',userRoutes)

const productRoutes = require('./routes/productRoutes');
console.log(productRoutes);
app.use('/products',productRoutes)

app.listen(port,()=>console.log(`server running at ${port}`))
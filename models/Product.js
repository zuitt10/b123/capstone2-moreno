const mongoose = require("mongoose");

	const productSchema = new mongoose.Schema({

			name: {
				type: String,
				required: [true, "name is Required"]
			},
			description: {
				type: String,
				required: [true, "description is Required"]
			},
			price: {
				type: Number,
				required: [true, "price is Required"]
			},
			isActive: {
				type: Boolean,
				default: true
			},
			createdOn: {
				type: Date,
				default: new Date()
				},

			orders: [{
					orderId: {
						type: String,
						required: [true, "Order Id is required"]
					},
					quantityOrder: {
						type: Number,
						required: [true,"Quantity Order is required"]
					}
		}] //end of subdocument orders array of object

}) //end of model

	module.exports = mongoose.model("Product", productSchema)
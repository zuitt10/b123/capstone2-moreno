const mongoose = require("mongoose");

	const userSchema = new mongoose.Schema({

			firstName: {
				type: String,
				required: [true, "First name is Required"]
			},
			lastName: {
				type: String,
				required: [true, "Last name is Required"]
			},
			mobileNo: {
				type: String,
				required: [true, "Mobile Number is Required"]
			},
			email: {
				type: String,
				required: [true, "Mobile Number is Required"]
			},
			password: {
				type: String,
				required: [true, "Password is Required"]
			},
			isAdmin: {
				type: Boolean,
				default: false
			},
			order: [{
				totalAmount: {
					type: Number,
					required: [true, "Total Amount is required"]
				},
				datePurchased: {
					type: Date,
					default: new Date()
				},
				products: [{

						productId: {
							type: String,
							required: [true, "Product Id is required"]
						},
						name: {
							type: String,
							required: [true, "name is required"]
						},
						quantity: {
							type: Number,
							required: [true, "quantity  is required"]
						},
						price:{
							type: Number,
							required: [true, "price  is required"]
						}

				}] // end of subdocument products array of object
		

		}] //end of subdocument order array of object

}) //end of model

	module.exports = mongoose.model("User", userSchema)
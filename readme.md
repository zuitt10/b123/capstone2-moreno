Project Name: The J Store E-Commerce API

Features:
	User Registration
	User Authentication
	Set user as admin (Admin only)
	Retrieve all active products
	Retrieve single product
	Crete Product (Admin Only)
	Update Product information (Admin Only)
	Archive Product (Admin Onle)
	Non-admin Product (Admin Only)
	Non-admin User checkout (Create order)
	Retrieve authenticated user's orders
	Retrieve all orders (Admin only)
	Activate Product (Admin Only)
	Display Products Per Order (User Only)

	Routes and request Body:
------------------------------------------------------------------------------------------------------
	Register
	Post - http://localhost:4000/register
------------------------------------------------------------------------------------------------------
	Body: (JSON)
	{
		"firstName": "String" ,
		"lastName": "String,
		"email": "String",
		"password": "String",
		"mobileNo": "String",
	}
------------------------------------------------------------------------------------------------------
	Login
	POST - http://localhost:4000/users/login
	Body: (JSON)
	{
		"email": "String",
		"password": "String"
	}

	Set as Admin
	PUT -http://localhost:4000/users/setUserAdmin/id
	Body : No request Body
	Admin token required.
------------------------------------------------------------------------------------------------------
	Create a Product
	POST - http://localhost:4000/products/createProduct
	Body : (JSON){
		
    "name": "String",
    "description":"String",
    "price": Number
	}
	Admin token Required
------------------------------------------------------------------------------------------------------
	Retrieve All Active Product
	GET - http://localhost:4000/products/retrieveActiveProduct
	Body: No request body.
------------------------------------------------------------------------------------------------------
	Retrieve a single product
	GET - http://localhost:4000/products/retrieveSingleProduct/id
	Body: No request body.
------------------------------------------------------------------------------------------------------
	Update a Product
	GET -http://localhost:4000/products/updateProduct/id
	Body: {
		"name": "String",
    	"description": "String",
    	"price": Number
		  }
	Admin Token Required
------------------------------------------------------------------------------------------------------
 	Order a Product
 	POST - http://localhost:4000/users/order
 	Body: {
    "totalAmount": "String,
    "products":
    [
    {
        "productId":"String",
        "name": "String",
        "quantity":Number,
        "price": Number  
    }
    ]
	}
------------------------------------------------------------------------------------------------------
	Archive a Product
	PUT - http://localhost:4000/products/archiveProduct/id
	Body: No request body.
	Admin Token Required
------------------------------------------------------------------------------------------------------
	Active a Product
	POST - http://localhost:4000/products/activeProduct/id
	Body: No request body.
	Admin Token Required
------------------------------------------------------------------------------------------------------
	Display Product Per Order
	GET - http://localhost:4000/products/displayProductPerOrder
	Body: No request body.
------------------------------------------------------------------------------------------------------
	Admin Credentials:
	email: "jericmoreno07@yahoo.com"
	password: "Ravenger1"
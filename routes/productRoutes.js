const express = require('express');
const router = express.Router();
const productControllers = require('../controllers/productControllers');
const { createProductController,
		retrieveActiveProduct,
		retrieveSingleProduct,
		updateProductController,
		archiveProductController,
		activateProductController,
		displayProductPerOrder
		} = productControllers;
const auth = require('../Authentication/auth');
const {verify,verifyAdmin} = auth;

// Create a product
router.post('/createProduct',verify,verifyAdmin,createProductController)

// retrieve all active courses
router.get('/retrieveActiveProduct', verify, retrieveActiveProduct)

// retrieve a single product
router.get('/retrieveSingleProduct/:id',verify, retrieveSingleProduct)
//Update a product
router.put('/updateProduct/:id',verify,verifyAdmin,updateProductController)
// Archive a product admin only
router.put('/archiveProduct/:id',verify,verifyAdmin,archiveProductController)
// Active a product admin only
router.put('/activeProduct/:id',verify,verifyAdmin,activateProductController)
//display products per order
router.get('/displayProductPerOrder',verify,displayProductPerOrder)
module.exports = router;
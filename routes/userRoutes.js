const express = require ('express');
const router = express.Router();
// add product controller here soon.
const userControllers = require ('../controllers/userControllers');
const { getSingleUserController,
		registerUserController,
	    loginUserController,
		setUserAdminController,
		order,
		viewOrderController,
		viewAllOrderController
	  } = userControllers;
const auth = require('../Authentication/auth');
const {verify,verifyAdmin} = auth;

//get single user
router.get('/getUserDetails',verify,getSingleUserController);
// Register a User
router.post('/register',registerUserController)
// Login
router.post('/login',loginUserController)
// Archive (admin)
router.put('/setUserAdmin/:id',verify,verifyAdmin,setUserAdminController)
// order for regular user
router.post('/order',verify,order)
// view orders for regular user
router.get('/viewOrder', verify, viewOrderController)
// view all orders (admin)
router.get('/viewAllOrder', verify,verifyAdmin,viewAllOrderController)


module.exports = router;